---
layout: page
title: Credits
permalink: /credits/
---

This course is inspired by the [Visualization Course](https://www.cs171.org/2023/) at the Harvard University, USA.
We are grateful for their support and borrowed parts of the course material taught by UPDATED.

We have heavily drawn on materials and examples found online and tried our best to give credit by linking to the original source. Please contact us if you find materials where the credit is missing or that you would rather have removed.


