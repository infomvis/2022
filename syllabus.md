---
layout: page
title: Syllabus
permalink: /syllabus/
menu: Syllabus
order: 2
---

## Course Goals
Information visualization is an increasingly important discipline for numerous scientific and application domains concerned with analysis and decision making with data. This course aims to prepare students to act as professionals able to execute the design, construction, and evaluation of information visualization pipelines, corresponding the following learning objectives:

- Identify what kind of problems visualization can solve
- Explain why and when visualization works
- Describe how to evaluate a visualization project: identify the elements of a project that need to be evaluated and strategies to carry out effective evaluations
- Use suitable visualization techniques for problems based on tasks and data types
- Be able to design and interpret interactive data visualizations,  and argue for their effectiveness

As such, this course is complementary to other courses, in particular data-science and machine-learning courses that train students on how to prepare datasets algorithmically.

## Assessment
The grade for INFOMVIS will be composed out of following grading components:

- Lecture examination, written exam (40%)
- Practical examination, final project in groups (60%)

The final grade is the weighted average of these two grade components. To pass the course, the final grade has to be at least a 5.5 (rounded to 6 in OSIRIS).

The written exam repair test requires at least a 4 for the original test.

## Course Form
- Lectures
- Self-study
- Labs with homeworks
- Project-work

## Late Policy

**Final project will not be accepted after the deadline (02.02.2024, 23:59).** 

## Devices in Class

We will use smartphones and laptops throughout the lecture to facilitate activities and project work in-class. However, research and student feedback clearly shows that using devices on non-class related activities not only harms your own learning, but other students' learning as well. Therefore, we only allow device usage during activities that require devices. At all other times, you should not be using your device. We will help you remember this by announcing when to bring devices out and when to put them away.

## Accessibility

If you have any concerns about accessibility please contact the TF(Başak Oral) or the Instructor(Michael Behrisch) as soon as possible. Failure to do so may prevent us from making appropriate arrangements.

### Textbooks

{% include_relative textbooks.md %}

### Discussion Forum

Discussion forums on [MS Teams (01_StudentsToStudents)](https://teams.microsoft.com/l/channel/19%3adb7b73a166ef4929b11bbd0689f42544%40thread.tacv2/01_StudentsToStudents?groupId=0f810a69-91cb-46d8-b8ab-10e1cd3ff632&tenantId=d72758a0-a446-4e0f-a0aa-4bf95a4a10e7), and on [MS Teams (02_StudentsToLecturers)](https://teams.microsoft.com/l/channel/19%3aa63eacf2ac0d4c2f9a2510cdfe3a0ded%40thread.tacv2/02_StudentsToLecturers?groupId=0f810a69-91cb-46d8-b8ab-10e1cd3ff632&tenantId=d72758a0-a446-4e0f-a0aa-4bf95a4a10e7)

Announcements are on [MS Teams (00_Announcements)](https://teams.microsoft.com/l/channel/19%3af77693a72ffb49a282310d2f35379703%40thread.tacv2/00_Announcements?groupId=0f810a69-91cb-46d8-b8ab-10e1cd3ff632&tenantId=d72758a0-a446-4e0f-a0aa-4bf95a4a10e7)

### Office Hours for Labs

Teaching fellows will provide office hours for individual questions that you might have about the lecture and practical group work, as well as general questions. As office hours are usually very heavily attended, please consult [MS Teams (02_StudentsToLecturers)](https://teams.microsoft.com/l/channel/19%3aa63eacf2ac0d4c2f9a2510cdfe3a0ded%40thread.tacv2/02_StudentsToLecturers?groupId=0f810a69-91cb-46d8-b8ab-10e1cd3ff632&tenantId=d72758a0-a446-4e0f-a0aa-4bf95a4a10e7) as a first option to get help.
