---
layout: home
title: Home
menu: Home
order: 1
---

<img src="assets/i/teaser.png">

## INFOMVIS 2023

Information visualization is an increasingly important discipline for numerous scientific and application domains concerned with analysis and decision making with data.

The amount and complexity of data produced in science, engineering, business, and everyday human activity is increasing at staggering rates.

In this course you will learn how the human visual system – a uniquely powerful system in our brain – can support reasoning over complex data as well as how to apply effective data visualization practices and methods.

In the lab sessions, you will work in groups and learn how to convey stories on a website using visualizations. You will learn web development technologies such as HTML, CSS, Javascript as well as a Javascript library (D3) for implementing visualizations.

## Theory Lectures:
Wednesdays 15:15 - 17:00, Location: [here]({{ site.baseurl }}/schedule/#lab_oh_schedule).

### Content

- Data Abstraction 
- Perception & Color
- Tasks & Interaction
- Evaluation
- Cognition
- Basic and Advanced Visualizations

## Labs:
Fridays 11:00 - 12:45, Location: [here]({{ site.baseurl }}/schedule/#lab_oh_schedule).

### Content

- HTML, CSS, DOM, Bootstrap
- Javascript
- SVG
- D3 (data binding, scales&axes, data update, linked views, custom visualization)

### Office Hours for labs: To be announced (Office hours)

Office hours will be posted [here]({{ site.baseurl }}/schedule/#lab_oh_schedule).

**Lecture Resources:**  
Discussion forums on [MS Teams (01_StudentsToStudents)](https://teams.microsoft.com/l/channel/19%3adb7b73a166ef4929b11bbd0689f42544%40thread.tacv2/01_StudentsToStudents?groupId=0f810a69-91cb-46d8-b8ab-10e1cd3ff632&tenantId=d72758a0-a446-4e0f-a0aa-4bf95a4a10e7), and on [MS Teams (02_StudentsToLecturers)](https://teams.microsoft.com/l/channel/19%3aa63eacf2ac0d4c2f9a2510cdfe3a0ded%40thread.tacv2/02_StudentsToLecturers?groupId=0f810a69-91cb-46d8-b8ab-10e1cd3ff632&tenantId=d72758a0-a446-4e0f-a0aa-4bf95a4a10e7)
  
Theory Lecture Materials are also on [MS Teams (General -> Files)](https://teams.microsoft.com/l/channel/19%3aS1UWvatMvv8O-iKn1QNKEr9ySvqA1lSrhf4KijuXoFo1%40thread.tacv2/General?groupId=0f810a69-91cb-46d8-b8ab-10e1cd3ff632&tenantId=d72758a0-a446-4e0f-a0aa-4bf95a4a10e7)

Announcements are on [MS Teams (00_Announcements)](https://teams.microsoft.com/l/channel/19%3af77693a72ffb49a282310d2f35379703%40thread.tacv2/00_Announcements?groupId=0f810a69-91cb-46d8-b8ab-10e1cd3ff632&tenantId=d72758a0-a446-4e0f-a0aa-4bf95a4a10e7)

## Instructors and TF
[Evanthia Dimara](https://www.evanthiadimara.com/home) (Instructor) \
[Michael Behrisch](http://michael.behrisch.info) (Instructor) \
[Başak Oral](https://basakoral.github.io/) (TF)
